# UCSC course 'Python for programmers'
Solutions for course's labs

### Lab1
* **Exercise 5** - Write a script that uses nested while loops to produce Christmass tree pattern

### Lab2
* **Exercise 1** - reads in two integers and determines whether the frst is a multiple of the second.
* **Exercise 2** - reads in integer and prints out the hexadecimal and octal representations of the number.
* **Exercise 3** - guesses user's number between lower_bound and upper_bound using binary search algorithm

### Lab3
* **Exercise 1** - use range operator to produce defined sequences
* **Exercise 2** - print sequence from 10 to 1 and 'BLASTOFF!!!' at the end
* **Exercise 4** - print 'Hi ya Manny!\nHi ya Moe!\nHi ya Jack!' using tuple of strings and loop
* **Exercise 5** - Prints the decimal equivalent of a binary string.

### Lab4
* **Exercise 1** - A coin flipping experiment
* **Exercise 3** - Pass quiz based ob flashcard and you'll get a feedback

### Lab5
* **Exercise 1** - Write a function that returns a total cost from the sales price and sales tax.
The default value for the tax rate should be 8.25%.
* **Exercise 2** - Write a Breakfast function that takes five arguments: meat, eggs,potatoes, toast, and beverage.
The default meat is bacon, eggs are over easy, potatoes is hash browns, toast is white, and beverage is coffee.
The function just says:
Here is your bacon and scrambled eggs with home fries and rye toast. Can I bring you more milk?
