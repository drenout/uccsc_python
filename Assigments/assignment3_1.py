"""Module presents classes Node and Tree

Node - class that presents node of tree
Tree - list of nodes, presenting tree structure

"""


class Node:
    """Class Node presents the component for building Tree class

    Attributes:
        name (str): presents name of node.
        self_id (int): presents unique id of the node
        parent_id (int): presents id of node's parent, if parent exists. Otherwise - None
    """

    def __init__(self, name, self_id, parent_id=None):
        if name and self_id:
            self.name = str(name)
            try:
                self.self_id = int(self_id)
            except ValueError:
                print 'self_id should be integer value'
                raise
        else:
            raise AttributeError("Node object should have name and self_id")
        if parent_id:
            try:
                self.parent_id = int(parent_id)
            except ValueError:
                print 'parent_id should be integer or empty value'
                raise
        else:
            self.parent_id = parent_id

    def __str__(self):
        return 'node name: %s, node id: %s, parent id: %s' % \
               (self.name, self.self_id, self.parent_id)

    def __eq__(self, other):
        return (self.name, self.self_id, self.parent_id) == (other.name, other.self_id, other.parent_id)

    def __ne__(self, other):
        return (self.name, self.self_id, self.parent_id) != (other.name, other.self_id, other.parent_id)

    def __hash__(self):
        return hash((self.name, self.self_id, self.parent_id))


class Tree:
    """Class Tree presents data structure made up of nodes and shows dependencies between them

    Attributes:
        data (list): list of Node objects, root is node with index 0.
    """
    def __init__(self, nodes=None):
        """Initialization of Tree object and creating structure

        Args:
            nodes (tuple): presents nodes for building tree
        """
        self.data = []
        if nodes is not None:
            if type(nodes[0]) != tuple:
                # Creating tree with 1 node only.
                self.data = [Node(*nodes)]
            else:
                # Creating tree with more than 1 nodes
                self.data = [Node(*node) for node in nodes]

    def __str__(self):
        """Print tree starting from the root"""
        if len(self.data) == 0:
            return ''

        def BuildTree(node, level=0):
            """Create printable view of tree

            Args:
                node (Node): from this node tree will be printed
                level (int): depth of the tree.

            """
            tree_view = '\t|' * level + str(node) + '\n'
            for child in self.data:
                if child.parent_id == node.self_id:
                    tree_view += '\t|' * (level + 1) + '\n'
                    tree_view += BuildTree(child, level + 1)
            return tree_view
        return BuildTree(self.data[0])

    def __iter__(self):
        """Iterate through tree structure"""
        for each in self.data:
            yield each

    def __repr__(self):
        """String representation of the Tree class"""
        if not self.data:
            return self.__class__.__name__ + '()'
        tree_keys = [str((key.name, key.self_id, key.parent_id)) for key in self.data]
        return self.__class__.__name__ + '((' + ', '.join(tree_keys) + '))'

    def __eq__(self, other):
        """Check if 2 trees are equal.

        Made sure that len of keys in structure is the same and every node from self is contained in other
        """
        if len(self.data) != len(other.data):
            return False
        for key in self.data:
            if key not in other.data:
                return False
        return True

    def AddNode(self, id, parent_id, name):
        """Add node to paren node using parent_id

        Args:
            id (int): presents unique id of the node which will be added
            name (str): presents name of the node
            parent_id (int): presents id of node from structure to whom new node will be added
        """
        parent_node = False
        for node in self.data:
            if node.self_id == id:
                print 'Cannot add node - node with same id is already in the tree'
                raise ValueError
            if node.self_id == parent_id:
                parent_node = True
        if not parent_node:
            print 'Cannot add node - parent for the node is not in the tree'
            raise ValueError
        self.data.append(Node(self_id=id, name=name, parent_id=parent_id))

    def DropNode(self, id, parent_id=None, name=None):
        """Delete leaf (node without children) from tree structure

        Args:
            id (int): presents unique id of the node which will be removed
            name (str): presents name of the node
            parent_id (int): presents id of node from which node will be removed
        """
        for node in self.data:
            if node.parent_id == id:
                print 'Cannot delete node with id=%d - node has children' % id
                raise ValueError
        node_in_tree = False
        for node in self.data:
            if node.self_id == id:
                node_in_tree = True
                self.data.remove(node)
        if not node_in_tree:
            print 'Cannot delete node with id=%d - node not in the tree' % id
            raise ValueError

    def DropFamily(self, id):
        """Remove node from tree structure with all children

        Args:
            id (int): presents unique id of the node which will be removed with all children
        """
        index_to_drop = []

        def NodesToDrop(id):
            # Store indexes of nodes that should be deleted
            for index, node in enumerate(self.data):
                if node.parent_id == id:
                    NodesToDrop(node.self_id)
                if node.self_id == id:
                    index_to_drop.append(index)
        NodesToDrop(id)
        for index in reversed(sorted(index_to_drop)):
            # Deleting nodes from the end
            self.data.pop(index)


def main():
    """main function for testing Tree class"""
    import assignment_data
    node1 = Node('node_1', 2)
    node2 = Node('node_1', 2, None)
    node3 = Node('node_3', 5)
    assert node1 == node2
    assert node1 != node3
    null_tree = Tree()
    print null_tree
    single_node = Tree(('Alice', 11, 9))
    print single_node
    the_tree = Tree(assignment_data.data)
    print the_tree
    for node in the_tree:  # testing the iterator
        print node
    the_tree.AddNode(id=14, parent_id=8, name="Lemon Drop")
    print "After AddNode(id=14):\n", the_tree
    the_tree.DropNode(id=14, parent_id=8, name="Lemon Drop")
    print "After Dropping it again:\n", the_tree
    try:
        the_tree.AddNode(id=14, parent_id=20, name="shouldn't work")
    except ValueError:
        pass
    try:
        the_tree.DropNode(id=88)
    except ValueError:
        pass
    the_tree.DropFamily(id=7)
    print "After Dropping the whole id=7 family:\n", the_tree
    assert eval(repr(the_tree)) == the_tree
    the_tree.DropFamily(the_tree.data[0].self_id)
    print 'After dropping from root: ', the_tree
    assert eval(repr(the_tree)) == Tree()


if __name__ == '__main__':
    main()
