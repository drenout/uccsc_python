"""Module find palindrome in files

SearchPalindromes - search palindromes in files

"""

import os
import sys

# Insert parent directory ro sys.path
if __name__ == '__main__':
    sys.path.insert(0, '..')
else:
    sys.path.insert(0, os.path.join(os.path.split(__file__)[0], '..'))

import utils.assignment2_2 as palindrome


def SearchPalindromes(start_dir):
    """Search palindromes in files from set directory

    Args:
        start_dir: parent dictionary from where palindromes will be searched in all files and subfolders.

    Returns:
        dictionary: key in dictionary is a palindrome string,
        value is set of file names where this palindrome was found.

    """
    palindrome_dict = {}
    for (cur_dir, sub_dir, files) in os.walk(start_dir):
        for file_name in files:
            full_path = os.path.join(cur_dir, file_name)
            try:
                open_file = open(full_path)
            except IOError as msg:
                print 'Cannot open file %s because %s' % (full_path, msg)
                return
            try:
                text = open_file.read().split()
                for word in text:
                    is_palindrome = palindrome.Palindromize(word)
                    if is_palindrome:
                        if is_palindrome not in palindrome_dict:
                            palindrome_dict[is_palindrome] = {full_path}
                        else:
                            palindrome_dict[is_palindrome].add(full_path)
            finally:
                open_file.close()
    return palindrome_dict


def main():
    """SearchPalindromes function is called from directory entered by user
    """
    start_dir = raw_input('Please enter starting directory: ')
    if not os.path.isdir(start_dir):
        sys.exit('Not existent directory was entered')
    result = SearchPalindromes(start_dir)
    for i in result:
        print '%s in %d files' % (i, len(result[i]))


if __name__ == '__main__':
    main()
