"""Module checks is works allowed Python identifier. By default words in DATA are checked

IsValidIdentifier - function that checks is word Python identifier or not
DATA - tuple of words that will be checked are they valid Python identifier or not

"""

from keyword import iskeyword


def IsValidIdentifier(word):
    """ Checks word for Python identifier

    Word is considered as valid identifier if:
    - The first character is a letter or underscore.
    - Letters beyond the first are alphanumeric or underscore.
    - Identifiers can not be keywords.

    Args:
        word: input string that will be checked is it Python identifier or not

    Returns:
        (True_or_False, reason_string) tuple is returned.
        (True, 'Valid') is set if word is valid Python identifier, (False, reason_why_false) otherwise.
    """
    if not (word[0].isalpha() or word[0] == '_'):
        return False, 'Invalid: first symbol must be alphabetic or underscore'
    for char in word[1:]:
        if not (char.isalnum() or char == '_'):
            return False, "Invalid: '%s' is not allowed." % char
    if iskeyword(word):
        return False, 'Invalid: this is a keyword!'
    return True, 'Valid'


DATA = ('x', '_x', '2x', 'x,y', 'yield', 'is_this_good')
# Tuple with test words for IsValidIdentifier function


if __name__ == '__main__':
    for word in DATA:
        print '%15s -> %s' % (word, IsValidIdentifier(word)[1])
