"""Module checks string for palindrome

Palindromize - checks if passed argument is palindrome or not

"""

import string


def Palindromize(phrase):
    """Checks if phrase is palindrome

    A phrase is a palindrome if, after all the whitespace and punctuation is removed,
    it is at least three characters long; and after it is made all the same case,
    it is the same backwards as forwards.

    Args:
        phrase: string that will be checked for palindrome

    Returns:
        the lowercase version of the phrase with whitespace and punctuation removed if the phrase is a palindrome,
        None otherwise
    """
    # Remove all punctuation and spaces
    palindrome = str(phrase).lower().translate(None, string.punctuation + ' ')
    if len(palindrome) > 2 and palindrome == palindrome[::-1]:
        return palindrome
    return None


def main():
    """Checks palindrome property for phrases in DATA have
    """
    DATA = ('Murder for a jar of red rum', 12321, 'nope', 'abcbA', 3443, 'what',
            'Never odd or even', 'Rats live on no evil star')
    for phrase in DATA:
        print Palindromize(phrase)


if __name__ == '__main__':
    main()
