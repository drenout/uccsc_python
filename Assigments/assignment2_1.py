"""Module converts integer values to ASCII characters

GiveAsciiChart - converts ASCII codes to characters
GiveAsciiChartShort - same functionality as in GiveAsciiChart, but wrote in one line
"""

import random

def GiveAsciiChart(data):
    """Converts list of ASCII codes to chars and print 4 values on a line

    Args:
        data: lint of integer ASCII codes that will be converted to characters and printed

    """
    for i, code in enumerate(data, 1):
        print '%-3s = %s' % (code, chr(code)),
        if not i % 4:
            print
    print


def GiveAsciiChartShort(data):
    """Converts list of ASCII codes to chars and print 4 values on a line

    Re-written version of GiveAsciiChart with as few lines as possible

    Args:
        data: lint of integer ASCII codes that will be converted to characters and printed

    """
    print '\n'.join([' '.join(['%-3s = %s' % (code, chr(code))
                               for code in data[i:i+4]]) for i in range(0, len(data), 4)])


def main():
    """Main flow of converting ASCII code to characters

    List of integer codes is randomly generated. List length is entered by user.
    Then generated list passed to GiveAsciiChartShort as argument

    """
    while True:
        data_len = raw_input('Please enter length of randomly generated list of ASCII code values between 32 and 126 '
                             'or empty string to exit: ')
        if not data_len:
            break
        try:
            data_len_int = int(data_len)
        except ValueError:
            print 'Entered value is not integer'
        else:
            data = [random.randint(32,126) for _ in range(data_len_int)]
            GiveAsciiChart(data)


if __name__ == '__main__':
    main()
    print 'Test data from assignment'
    ASSIGNMENT_DATA = [32, 56, 80, 104, 33, 57, 81, 105, 34, 58, 82, 106]
    GiveAsciiChartShort(ASSIGNMENT_DATA)
