"""Module takes a starting url ('www.ngc.com'), finds all the email addresses on that page,
and all the links to urls on that page, and then reads all the links, and their links, etc.,
collecting email addresses.
"""

import re
import requests

EMAILS = re.compile(r"""(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])""")
LINKS = re.compile(r"http://[^\s\"<>()']*\b")


def find_mails_on_page(page, mail_list):
    try:
        response = requests.get(page)
    except requests.exceptions.ConnectionError:
        print 'could not connect to ' + page
        return
    mail_list += EMAILS.findall(response.text)
    for link in LINKS.findall(response.text):
        find_mails_on_page(link, mail_list)


def main():
    start_url = 'http://goo.gl'
    mail_list = []
    find_mails_on_page(start_url, mail_list)
    print(mail_list)


if __name__ == '__main__':
    main()
