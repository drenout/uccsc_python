""""
Lab3 exercises
exercise1 - use range operator to produce defined sequences
exercise2 - print sequence from 10 to 1 and 'BLASTOFF!!!' at the end
exercise4 - print 'Hi ya Manny!\nHi ya Moe!\nHi ya Jack!' using tuple of strings and loop
exercise5 - Prints the decimal equivalent of a binary string.
"""


def exercise1():
    print range(3, 13, 3)
    print range(-10, 211, 110)
    for num in range(-1, -8, -2):
        print "%d," % num,


def exercise2():
    for num in range(10,0,-1):
        print "%d," % num,
    else:
        print "BLASTOFF!!!"


def exercise4():
    for name in ("Manny", "Moe", "Jack"):
        print "Hi ya %s!" % name


def exercise5():
    bin_str = raw_input("Binary string: ")
    for ch in bin_str:
        if ch not in "01":
            print "Number is not binary"
            return
    # while method
    result, power, bin_str_copy = 0, 0, bin_str
    while bin_str_copy:
        result += int(bin_str_copy[-1]) * (2 ** power)
        power += 1
        bin_str_copy = bin_str_copy[:-1]
    print "While loop result: %d" % result

    # for method
    result, power = 0, 0
    for ch in bin_str[::-1]:
        result += int(ch) * (2 ** power)
        power += 1
    print "For loop result: %d" % result

    # easy method
    print "Decimal equ" \
          "ivalent: %d" % int(bin_str, 2)


exercise1()
