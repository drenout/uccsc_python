"""The module reads current directory and calculate size of files
"""

import os
from subprocess import Popen, PIPE
import re


def is_date(string):
    return bool(re.search(r'(\d+/\d+/\d+)', string))


def calculate_size_of_files(path):
    process = Popen(["cmd", "/c", "dir", path], stdout=PIPE).stdout
    total = 0
    for line in process:
        parts = line.split()
        print parts
        # Files and folders have date in Windows
        if len(parts) == 5 and is_date(parts[0]):
            try:
                total += int(parts[3].replace(',', ''))
            except ValueError:
                # Folder was found
                pass
    return total


def get_size_of_files(path):
    # Checking size of files via os.path.getsize()
    files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    total = 0
    for i in files:
        total += os.path.getsize(i)
    return total


def main():
    path = '.'
    print 'My total = ', calculate_size_of_files(path)
    print 'getsize total = ', get_size_of_files(path)


if __name__ == '__main__':
    main()
