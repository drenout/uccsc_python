"""Replace words in zen.story file according to dictionary replace_d
"""

import re


def match_case(answer, like_string):
    if like_string.isupper():
        return answer.upper()
    if like_string.islower():
        return answer.lower()
    if like_string.istitle():
        return answer.title()
    return answer


def replace_in_file(replace_d, f_name):
    def do_swap(match_object):
        element = match_object.group()
        return match_case(replace_d[element.lower()], element)
    try:
        with open(f_name) as open_file:
            pattern = re.compile(r'\b(' + '|'.join(replace_d.keys()) + r')\b', re.IGNORECASE)
            for line in open_file:
                print pattern.sub(do_swap, line),
    except IOError:
        print 'File couldn\'t be opened'
        exit()


def main():
    replace_d = {
        'general': 'band leader',
        'china': 'mexico',
        'zen': 'mariachi',
        'master': 'teacher',
        'sword': 'baton',
        'through': 'around'}
    name = raw_input('Please specify file name: ')
    replace_in_file(replace_d, name)


if __name__ == '__main__':
    main()
