"""Module runs ipconfig and prints out your IP address
"""

from subprocess import Popen, PIPE
import re

if __name__ == '__main__':
    process = Popen(["cmd", "/c", "ipconfig", "/all"], stdout=PIPE)
    match = re.search(r'IPv4.*: (\d+\.\d+\.\d+.\d+)', process.communicate()[0])
    if match:
        print 'IPv4 addres:', match.group(1)
