"""Module opens all files in directory and subdirectories and counts how many times each word appears in files
"""

import os
import Lab10_exercise3


def count_words_in_dir(root):
    all_words = {}
    for (cur_dir, sub_dir, files) in os.walk(root):
        for open_file in files:
            full_path = os.path.join(cur_dir, open_file)
            file_words = Lab10_exercise3.count_words(full_path)
            for word in file_words:
                if word in all_words:
                    all_words[word] += file_words[word]
                else:
                    all_words[word] = file_words[word]
    return all_words


if __name__ == '__main__':
    Lab10_exercise3.top_10(count_words_in_dir(r'D:\Documents\UCSC Python\3064_Davis_labs\labs\lab_10_File_IO\cats'))
