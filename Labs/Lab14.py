"""Module for Lab14
Stack class - simple stack realisation
"""


class Stack:
    def __init__(self):
        self.stack = []

    def push(self, element):
        self.stack.append(element)

    def pop(self):
        try:
            return self.stack.pop()
        except IndexError, e:
            print e


class Employee:
    def __init__(self, name='John'):
        self.name = name
        self.salary = 0

    def PrintName(self):
        print self.name,

    def GiveRaise(self, percent):
        increase = self.salary / 100 * float(percent)
        self.salary += increase
        return increase


class SalariedEmployee(Employee):
    def __init__(self, name):
        Employee.__init__(self, name)

    def SetSalary(self, salary):
        try:
            self.salary = float(salary)
        except ValueError as e:
            print e

    def CalculatePay(self, weeks):
        return self.salary / 52 * float(weeks)


class ContractEmployee(Employee):
    def __init__(self, name):
        Employee.__init__(self, name)

    def SetRate(self, hour_rate):
        try:
            self.salary = float(hour_rate)
        except ValueError as e:
            print e

    def CalculatePay(self, hours):
        return self.salary * float(hours)



if __name__ == '__main__':
    # example = Stack()
    # for i in range(10):
    #     example.push(i)
    # print example.pop()
    joe = SalariedEmployee('Joe')
    joe.SetSalary(52000)  # Joe is salaried so this is his yearly salary
    joe.PrintName()
    print "here's $%.2f for you. " % joe.CalculatePay(1)  # 1 week
    joe.GiveRaise(2)  # A 2% raise!
    joe.PrintName()
    print "here's $%.2f for you. " % joe.CalculatePay(2)  # 2 weeks
    susan = ContractEmployee('Susan')
    susan.PrintName()
    susan.SetRate(100)  # Susan is contract so this is her hourly pay
    print "here's $%.2f for you. " % susan.CalculatePay(80)
    susan.GiveRaise(2)  # A 2% raise!
    susan.PrintName()
    print "here's $%.2f for you. " % susan.CalculatePay(80)  # 80 hours
