"""
Module uses words from DATA to convert alphabet characters to numbers according to phone pad

PHONE_PAD - correspondence of alphabet characters to numbers o phone pad
DATA - predefined input data for replacement alphabet characters
word_to_keypad - function that replaces alphabet characters to numbers
"""

PHONE_PAD = ('abc', 2), ('def', 3), ('ghi', 4), ('jkl', 5), ('mno', 6), ('pqrs', 7), ('tuv', 8), ('wxyz', 9)
DATA = 'peanut', 'salt', 'lemonade', 'good time', ':10', 'Zilch'


def word_to_keypad(word):
    """Replace characters in word according to numbers on phone pad

    :param word: word, which alphabet characters will be replaced to numbers
    :return new_word: modified word with numbers instead alphabet characters. Non-alpha characters are left as it is
    """
    new_word = ''
    for char in word:
        if char.isalpha():
            for key in PHONE_PAD:
                if char in key[0]:
                    char = str(key[1])
                    break
        new_word += char
    return new_word


if __name__ == '__main__':
    for word in DATA:
        print word_to_keypad(word)
