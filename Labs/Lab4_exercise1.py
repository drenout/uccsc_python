"""
Lab4 exercise1
A coin flipping experiment
"""

import random


def toss_coin():
    """function emulates the flip of a coin, returning "heads" or "tails"""

    return random.choice(("head", "tail"))


def experiment():
    """function flips coins until it gets three heads in a row"""

    attempts, heads_in_raw = 0, 0
    while heads_in_raw < 3:
        if toss_coin() == "head":
            heads_in_raw += 1
        else:
            heads_in_raw = 0
        attempts += 1
    return attempts


if __name__ == "__main__":
    total_num = 0
    for _ in range(10):
        total_num += experiment()
    print "Average number of flips in 10 experiments is {0}".format(total_num/10.0)
