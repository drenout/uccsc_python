"""Unit tests for Clock and Money classes
"""

import sys
import os
import unittest
from datetime import datetime

if __name__ == '__main__':
    sys.path.insert(0, '.')
else:
    sys.path.insert(0, os.path.split(__file__)[0])

from Lab16_exercise2 import Money
from Lab15 import Clock


class TestMoney(unittest.TestCase):

    def testFormat(self):
        self.assertEqual(str(Money(-123.21)), '-$123.21')
        self.assertEqual(str(Money(40.50)), '$40.50')
        self.assertEqual(str(Money(-1001.011)), '-$1,001.01')
        self.assertEqual(str(Money(123456789.999)), '$123,456,790.00')
        self.assertEqual(str(Money(.10)), '$0.10')
        self.assertEqual(str(Money(.01)), '$0.01')
        self.assertEqual(str(Money(.055)), '$0.06')

    def testRepr(self):
        self.assertTrue(eval(repr(Money(44.123))) == Money(44.123))

    def testMathOperations(self):
        self.assertTrue(Money(10) + Money(20) == Money(30))
        self.assertAlmostEquals(Money(44.333) - Money(55.444), Money(-11.111))
        self.assertTrue(2 * Money(-11.11) == Money(-22.22) == Money(11.11) * -2)
        self.assertTrue((Money(44.44)) / 4 == Money(11.11))

    def testNeg(self):
        self.assertTrue(-Money(10.00) == Money(-10.00))


class TestClock(unittest.TestCase):

    def testFormat(self):
        self.assertEqual(str(Clock(min=1, hr=9)), '09:01')
        self.assertEqual(str(Clock("12:50")), '12:50')
        self.assertEqual(str(Clock((1, 10))), '01:10')
        self.assertEqual(str(Clock({'hr': 2, 'min': 30})), '02:30')
        date = datetime.strftime(datetime.now(), "%I:%M")
        self.assertEqual(str(Clock()), date)

    def testRepr(self):
        self.assertTrue(eval(repr(Clock(11, 48))) == Clock(11, 48))

    def testAddSub(self):
        self.assertTrue(Clock(12, 50) + Clock(3, 30) == Clock(4, 20))
        self.assertTrue(Clock(1, 20) - Clock(4, 50) == Clock(8, 30))


if __name__ == '__main__':
    unittest.main()
