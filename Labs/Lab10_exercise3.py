"""Module opens file and counts how many times each word appears in text
"""

import string


def count_words(file_name):
    """Open file and calculate the number of occurrences of every word"""
    words = {}
    with open(file_name) as open_file:
        for line in open_file:
            for word in line.lower().translate(None, string.punctuation).split():
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
    return words


def top_10(words):
    """Print top 10 words and word's score"""
    total_words = sum(words.values())
    top_words = sorted(words.items(), key=lambda x: x[1], reverse=True)
    i = 0
    while i < 10 and i < len(top_words):
        print 'Word \'%s\' appears %s times in text. Word\'s score is %.4f' \
              % (top_words[i][0], top_words[i][1], top_words[i][1] / float(total_words))
        i += 1
    """Print remaining words with same score on 10th place"""
    while i < len(top_words) and top_words[9][1] == top_words[i][1]:
        print 'Word \'%s\' appears %s times in text. Word\'s score is %.4f' \
              % (top_words[i][0], top_words[i][1], top_words[i][1] / float(total_words))
        i += 1


if __name__ == '__main__':
    top_10(count_words(r'D:\Documents\UCSC Python\3064_Davis_labs\labs\lab_10_File_IO\zen.story'))
    print __doc__

