"""
Lab 5
exercise1
Write a function that returns a total cost from the sales price and sales tax.
The default value for the tax rate should be 8.25%.

exercise2
Write a Breakfast function that takes five arguments: meat, eggs,potatoes, toast, and beverage.
The default meat is bacon, eggs are over easy, potatoes is hash browns, toast is white, and beverage is coffee.
The function just says:
Here is your bacon and scrambled eggs with home fries and rye toast. Can I bring you more milk?

"""


def total_cost(price, tax=8.25):
    # Function returns total cost the sales price and sales tax rate
    return price + price * tax / 100.0


def breakfast(meat='bacon', eggs='over easy', potatoes='hash browns', bread='white', beverage='coffee'):
    # Function prints breakfast order according to input ingredients
    print '"Here is your {0} and {1} eggs with {2} and {3} toast. Can I bring you more {4}?'\
        .format(meat, eggs, potatoes, bread, beverage)


if __name__ == '__main__':
    # Called total_cost function 2 times with default and non-default values
    print total_cost(100)
    print total_cost(200, 20)
    # Called breakfast function 4 times with different input values and in different order
    breakfast()
    breakfast(eggs='raw', bread='black', meat='lamb', beverage='tea', potatoes='mashed potatoes')
    breakfast(beverage='juice', bread='grey', potatoes='home made potatoes', eggs='omelet', meat='sausages')
    breakfast('pork', beverage='water')
