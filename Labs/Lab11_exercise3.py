"""Emulates the old Linux tree command from defined by user directory

print_tree - print Linux tree
build_tree - create dictionary based on folders structure
"""

import os


def print_tree(tree, cur_dir, level=2):
    """Print Linux tree

    Function prints sorted by name objects (subdirectories and files) of current folder and
    if object is a directory, calls print_tree for this directory and level+1


    Args:
        tree: tree structure with all folders and files
        cur_dir: current parent directory
        level: level of cur_dir relatively to start directory
    """
    print '\t|' * level + '-- /' + os.path.split(cur_dir)[1]
    level += 1
    for obj in tree[cur_dir]:
        if os.path.isfile(os.path.join(cur_dir, obj)):
            print '\t|' * level + '-- ' + obj
        else:
            print_tree(tree, os.path.join(cur_dir, obj), level)


def build_tree(start_dir):
    """Build dictionary with folders and files

    Args:
        start_dir: directory, defined by user, concerning which the tree will be built

    Returns:
        dictionary where key is folder, value is a list of files and subdirectories sorted by name

    """
    tree = {}
    for (cur_dir, sub_dir, files) in os.walk(start_dir):
        tree[cur_dir] = sorted(sub_dir + files)
    return tree


def main():
    tree = build_tree(raw_input('Please enter start directory to build the tree: '))
    start_at = sorted(tree)[0]
    print_tree(tree, start_at)


if __name__ == '__main__':
    main()
