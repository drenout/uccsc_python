"""Lab8 exercises
exercise2 - generate_deck() produces deck of cards
exercise3 - matrix_generator() use list comprehensions to produce matrix 6*6 with the fewest possible lines of code
"""


def generate_deck():
    """Generates deck of 54 cards"""
    print 'The deck contains:'
    suits = 'Clubs', 'Diamonds', 'Heart', 'Spades'
    rank = [str(x) for x in range(2, 11)] + ['Jack', 'Queen', 'King', 'Ace']
    cards = [x + ' of ' + y for y in suits for x in rank] + ['Joker'] * 2
    return cards


def matrix_generator():
    # My solution
    matrix = [[x * y for x in range(6)] for y in range(6)]
    for elem in matrix:
        print '%s' % '\t'.join(map(str, elem))
    # Marilin's solution
    print '\n'.join([''.join(['%4d' % (n * m)
                              for n in range(6)]) for m in range(6)])


def make_money(amount):
    return '{0}{1:,.2f}'.format('$' if amount >= 0 else '-$', abs(amount))


if __name__ == '__main__':
    # deck = generate_deck()
    # for i, card in enumerate(deck[:-1]):
    #     print '%s,' % card,
    #     if i % 4 == 3:
    #         print
    # print 'and %s' % deck[-1]
    # matrix_generator()
    print make_money(-1000.22)