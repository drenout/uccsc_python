"""Signals handling
"""
import time
import signal


class TimeOut:

    def __init__(self, timeout=0):
        self.timeout = timeout

    def __enter__(self):
        def receive_alarm(signum, stack):
            raise RuntimeError, 'Time out after %s seconds' % self.timeout

        self.old = signal.signal(signal.SIGALRM, receive_alarm)
        signal.alarm(self.timeout)

    def __exit__(self, exc_type, exc_val, exc_tb):
        signal.signal(signal.SIGALRM, self.old)
        signal.alarm(0)


def main():
    with TimeOut(2) as ticker:
        try:
            time.sleep(5)
        except RuntimeError:
            print "Sleeping 5 timed out!"

    with TimeOut(5) as ticker:
        try:
            time.sleep(2)
            print "Sleeping 2 didn't time out."
        except RuntimeError:
            print "Timed out!"


if __name__ == '__main__':
    main()