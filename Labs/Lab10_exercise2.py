"""Module goes through directories and swaps cat to dog in all files
"""

import os
import Lab10_exercise1


def swap_in_dir(root):
    for (cur_dir, sub_dir, files) in os.walk(root):
        for open_file in files:
            full_path = os.path.join(os.path.abspath(cur_dir), open_file)
            Lab10_exercise1.swap_in_file(full_path)
            print 'Replacement was made in %s' % full_path


if __name__ == '__main__':
    swap_in_dir(r'D:\Documents\UCSC Python\3064_Davis_labs\labs\lab_10_File_IO\cats')
