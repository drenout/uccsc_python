"""Module which opens file and replaces all cat to dog using do_swap module
"""

import do_swap


def swap_in_file(file_path):
    """Swap all cat to dogs in file """
    data = ''
    with open(file_path) as open_file:
        for line in open_file:
            data += do_swap.DoSwap(line, 'cat', 'dog')

    with open(file_path, 'w') as open_file:
        open_file.writelines(data)


if __name__ == '__main__':
    swap_in_file(r'D:\Documents\UCSC Python\3064_Davis_labs\labs\lab_10_File_IO\cats.txt')
