"""Lab18
UpIt(str) function returns the input string, but with all caps
 Get_XY(prompt) - returns 2 float numbers from user input
 """

import math


class UpperChar(ValueError):
    def __str__(self):
        return 'All chars should be lower case in %s' % self.args


def UpIt(input):
    if input is '':
        return 'Please enter the word'
    if not input.islower():
        raise UpperChar, input
    return input.upper()


def Get_XY(prompt):
    while True:
        try:
            input = raw_input(prompt)
        except (KeyboardInterrupt, EOFError):
            print 'Client interrupted program'
            return None
        if input == '' or input.lower() == 'quit':
            print 'Exiting the program'
            return None
        try:
            x, y = [float(x) for x in input.split()]
        except (TypeError, ValueError, NameError, SyntaxError):
            print '2 numbers are required. Please try again: '
        else:
            return x, y


def main():
    # try:
    #     print UpIt(str(raw_input('Please enter lowercase word: ')))
    # except UpperChar as msg:
    #     print msg
    xy = Get_XY("Please enter 2 sides of right triangular via space: ")
    print(math.hypot(xy[0], xy[1]))


if __name__ == '__main__':
    main()
