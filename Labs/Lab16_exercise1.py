"""Module for Sorted dictionary
"""


class SortedDictionary(dict):

    allowed_attributes = 'description',

    def __init__(self, data, description=None):
        dict.__init__(self, data)
        self.__dict__['description'] = description

    # def __getitem__(self, item):
    #     return dict.__getitem__(self, item)

    def __setattr__(self, key, value):
        if key not in SortedDictionary.allowed_attributes:
            raise NameError("You can't change the %s to %s" % (key, value))
        else:
            self.__dict__[key] = value

    def __iter__(self):
        for i in self.keys():
            yield i

    def keys(self):
        return sorted(dict.keys(self))


if __name__ == '__main__':
    d = SortedDictionary({5: '1', 2: '2', 17: '3', 6: '4'}, 'test')
    d1 = SortedDictionary({})
    d2 = SortedDictionary({}, description='test2')
    d3 = SortedDictionary(((1, '1'), (2, '2')))
    print 'd:', d, d.keys(), d.description
    print 'd1:', d1, d1.keys(), d1.description
    print 'd2:', d2, d2.keys(), d2.description
    print 'd3:', d3, d3.keys(), d3.description
    # d.descriptionit = 'qwe'
    d.description = 'qwe'
    print d.description
    for i in d:
        print i
