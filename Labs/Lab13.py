"""
Exercise 1:
Printf - Write a Printf() function for Python that behaves like printf in C
Exercise 2:
madlib_replacement -  Function that takes in any random madlib and returns the madlib with the responses filled in
"""

import random
import time

def GetCards():
    """Return a deck of cards as a list of strings."""

    values = [str(x) for x in range(2, 11)] + ['Jack','Queen','King','Ace']
    suits = ('Clubs', 'Diamonds', 'Hearts', 'Spades')
    deck = [v + ' of ' +  s for s in suits for v in values] + ["Joker"] * 2
    return deck

def Printf(format, *args):
    # my solution
    # exec('print "'+ str(args[0]) +'" % ' + str(args[1:]) )
    print format % args


def madlib_replacement(madlib):
    words = ()
    phrase = madlib.split()
    for index, word in enumerate(phrase):
        if word[0] == '%':
            words += (raw_input('Please enter %s ' % word.split(')')[0][2:]), )
            phrase[index] = '%s'
    return ' '.join(phrase) % words

def deal_card():
    deck = GetCards()
    random.shuffle(deck)
    for card in deck:
        yield card


def deal_hand(cards_num=4):
    deck = deal_card()
    while True:
        hand = []
        for _ in range(cards_num):
            try:
                hand.append(deck.next())
            except StopIteration:
                hand.append(None)
        yield hand


def DealGame(hands=4, cards=5):
    game = []
    deal = deal_hand(cards)
    for i in range(hands):
        game += [deal.next()]
    for i, hand in enumerate(game):
        print 'Hand #%s: %s' % (i, hand)


def time_decorator(func):
    """Decorator function for reporting when the function was called."""
    def wrapper(*t_args, **kw_args):
        with open('lotto.log', 'a') as log_file:
            log_file.write('%s -> %s\n' % (time.ctime(), func(*t_args, **kw_args)))
        # print '%s -> %s' % (time.ctime(), lotto_wrapper(func))
    return wrapper


def lotto_wrapper(lotto_func):
    def wrapper(*t_args, **kw_args):
        result = lotto_func(*t_args, **kw_args)
        return ', '.join(str(x) for x in result)
    return wrapper


@time_decorator
@lotto_wrapper
def Lotto():
    return random.sample(range(1, 53), 6)


if __name__ == '__main__':
    MADLIB = """After trying to %(verb)s around the %(noun)s %(number)s times,
    we finally %(past_tense_verb)s the %(plural_noun)s instead."""
    # Printf("%s slid %d times.", "John", 3)
    # Printf("%s and %s ate %d %s.", "Lynn", "Mary", 22, "grapes")
    # print madlib_replacement(MADLIB)
    # DealGame(4, 4)
    # print Lotto()
    file_object = open("lotto.log", "w")

    file_object.write('This is a test')
    file_object.write('To add more lineffffffffffffffffffffs.')

    # file_object.close()