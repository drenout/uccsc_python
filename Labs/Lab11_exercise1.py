"""
 Write a function that reads a file and finds all the 4 numbers in the file and adds them up.
"""

import os
import sys

if __name__ == '__main__':
    sys.path.insert(0, '..')
else:
    sys.path.insert(0, os.path.join(os.path.split(__file__)[0], '..'))

import banana.total_text


def total_in_file(file_name):
    try:
        with open(file_name) as open_file:
            total = 0
            for line in open_file:
                total += banana.total_text.TotalText(line)
            return total
    except IOError:
        print 'File couldn\'t be opened'
        exit()


def main():
    name = raw_input('Please specify file name: ')
    if name == '':
        exit()
    print name, 'totals to', total_in_file(name)


if __name__ == '__main__':
    main()
