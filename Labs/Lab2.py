"""
Lab2 exercises
exercise1 - reads in two integers and determines whether the frst is a multiple of the second.
exercise2 - reads in integer and prints out the hexadecimal and octal representations of the number.
exercise3 - guesses user's number between lower_bound and upper_bound using binary search algorithm
"""


def exercise1():
    try:
        first_int = int(raw_input('Please enter first integer '))
        second_int = int(raw_input('Please enter second integer '))
        remainder = second_int % first_int
    except ValueError:
        print 'Numbers are not integer'
    except ZeroDivisionError:
        print 'We cannot divide to zero'
    else:
        print 'First is a multiple of second' if not remainder else 'First is not a multiple of second'


def exercise2():
    try:
        int_number = int(raw_input('Please enter integer number '))
    except ValueError:
        print 'Number is not integer'
    else:
        print 'hex representation is %#x\noct representation is %#o' % (int_number, int_number)


def exercise3(lower_bound, upper_bound):
    print "Think of a number between %d and %d and I'll try to guess it." % (lower_bound, upper_bound)
    guesses = 1
    while lower_bound != upper_bound:
        guess_value = (upper_bound + lower_bound) / 2
        print 'Is your number %d' % guess_value
        answer = raw_input('''Please press:
        'y' for yes
        'n' for no
        ''')
        if answer == 'y':
            print 'Hurray! Only %d %s.' % (guesses, 'guesses' if guesses > 1 else 'guess')
            return
        answer = raw_input('''No? Then please press:
        'h' if %d is higher than your number
        'l' if %d is lower than your number
        ''' % (guess_value, guess_value))
        if answer == 'h':
            upper_bound = guess_value
        else:
            lower_bound = guess_value + 1
        guesses += 1


exercise3(1, 10)