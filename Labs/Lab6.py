""""
Lab3 exercises
exercise2 - Ask the user for 5 numbers, one at a time.
As each number is read, print it only if it is not a duplicate of a number already read.
exercise3 - Sort predefined list by last name and print them, last name first.
exercise4 - program inputs a line of text and translates it to Pig Latin
"""


def exercise2(num_amount):
    while True:
        try:
            numbers = map(int, raw_input('Please enter {0} numbers using space: '.format(num_amount)).split())
            if len(numbers) == num_amount:
                break
            else:
                print 'You entered not exact %d numbers' % num_amount
        except ValueError:
            print 'You entered not number values'
    for num in set(numbers):
        print num,


def name_swap(full_name):
    # function swaps first and last names
    full_name_list = full_name.split()
    return full_name_list[-1] + ', ' + ' '.join(full_name_list[:-1])


def exercise3(names):
    for name in sorted(names, key=name_swap):
        print name_swap(name)


def exercise4():
    pig_latin = []
    for word in raw_input('Tell me something: ').split():
        pig_latin += [to_pig(word)]
    print ' '.join(pig_latin)


def to_pig(word):
    # Check if firs letter is in upper case
    upper = word[0].isupper()
    # Variable for punctuation mark
    char = False
    if word[-1] in ',.;:':
        char = word[-1]
        word = word[:-1]
    if word[0] in 'aeouyAEOUY':
        word = word + 'way'
    else:
        prefix = 0
        while word[prefix] not in 'aeouyAEOUY' and prefix < len(word) - 1:
            prefix += 1
        word = word[prefix:] + word[:prefix] + 'ay'
    if upper:
        word = word.title()
    if char:
        word += char
    return word


if __name__ == '__main__':
    # exercise2(5)
    # NAMES = ["Jack Sparrow", "George Washington", "Tiny Sparrow", "Jean Ann Kennedy"]
    # exercise3(NAMES)
    exercise4()
