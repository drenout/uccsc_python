"""
Exercise 5
"""

HEIGHT = 10
line=0
while line < HEIGHT:
    print ' ' * (HEIGHT - 1 - line) + '*' * (1 + line*2) + ' ' * (HEIGHT - 1 - line)
    line += 1