"""Lab13 exercise 3 with optparse module
"""

import Lab13
import optparse

def main():
    parser = optparse.OptionParser(
        """%prog [-p players=4] [-c cards=5]
        Deals defined number of cards to players."""
    )
    parser.add_option('-p', '--players', dest='players', default=4, help='Define number of players.')
    parser.add_option('-c', '--cards', dest='cards', default=5, help='Define number of cards per player.')
    (options, args) = parser.parse_args()
    print options, args
    if len(args) > 0:
        parser.error('Arguments %s are not recognized' % (' '.join(args)))
    Lab13.DealGame(int(options.players), int(options.cards))


if __name__ == '__main__':
    main()
