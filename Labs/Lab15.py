"""Module for exercise in Lab15
Class Clock
"""

from datetime import datetime


class Clock:
    max_min = 60
    max_hour = 12

    def __init__(self, hr=None, min=None):
        if hr is None and min is None:
            date = datetime.strftime(datetime.now(), "%I:%M")
            self.hr, self.min = map(int, date.split(':'))
        if type(hr) == int and type(min) == int:
            self.__check_clock(self, hr, min)
        if type(hr) == str and min is None:
            try:
                hr, min = map(int, hr.split(':'))
                self.__check_clock(self, hr, min)
            except ValueError as e:
                print 'Couldn\'t convert string to clock:'
                raise Exception(e)
        if type(hr) == tuple and min is None:
            hr, min = hr
            self.__check_clock(self, hr, min)
        if type(hr) == dict and min is None:
            min = hr['min']
            hr = hr['hr']
            self.__check_clock(self, hr, min)

    @staticmethod
    def __check_clock(self, hr, min):
        if hr > Clock.max_hour:
            raise Exception('Hour value should be less that %d. %d hours were set' % (Clock.max_hour, hr))
        if min >= Clock.max_min:
            raise Exception('Hour value should be less that %d. %d minutes were set' % (Clock.max_min, min))
        self.hr = hr
        self.min = min

    def __str__(self):
        return '%02d:%02d' % (self.hr, self.min)

    def __add__(self, other):
        min = (self.min + other.min) % Clock.max_min
        hr = 1 + ((self.hr + other.hr + (self.min + other.min) / Clock.max_min - 1) % Clock.max_hour)
        return Clock(hr, min)

    def __sub__(self, other):
        min = (self.min - other.min) % Clock.max_min
        hr = 1 + ((self.hr - other.hr + (self.min - other.min) / Clock.max_min - 1) % Clock.max_hour)
        return Clock(hr, min)

    def __repr__(self):
        return self.__class__.__name__ + "('%s')" % str(self)

    def __eq__(self, other):
        if self.hr == other.hr and self.min == other.min:
            return True
        return False

    def minute_since_12(self):
        return (self.hr % 12) * 60 + self.min


if __name__ == '__main__':
    x = Clock(min=1, hr=9)
    y = Clock("12:50")
    z = Clock((1, 10))
    w = Clock({'hr': 2, 'min': 30})
    r = Clock()
    t1 = eval(repr(Clock(11, 48)))
    t2 = Clock(11, 48)
    assert t1 == t2
    xx = Clock(1, 20) - Clock(4, 50)
    print xx
    v = y - z
    print x, y, z, w, r
    print v, v.minute_since_12()

