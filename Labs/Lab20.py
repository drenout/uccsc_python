"""Lab 20 - exercise with regex
"""

import re


def raise_salary(data, amount=250):
    salary = re.compile(r'(\d+)$', re.MULTILINE)

    def raise_salary(match_object):
        return str(int(match_object.group()) + amount)
    return salary.sub(raise_salary, data)


def names_numbers(data, code=408):
    name_numb = re.compile(r'^(.*?)[:]408-([-,\d]+)', re.MULTILINE)
    return name_numb.findall(data)


def california(data):
    return re.sub(r'CA (\d)', r'California \g<1>', data)


def month_of_birthday(data, month=4):
    return re.findall(r'(^.+?):.*:%s\/' % month, data, re.MULTILINE)


def not_word(data, word='Boop'):
    return re.findall(r'^(?!.*?%s.*?)(.*?)$' % word, data, re.MULTILINE)


def reverse_name(data):
    return re.sub(r'(\w+) (\w+):', r'\g<2>, \g<1>:', data)

def main():
    try:
        with open(r'D:\Documents\UCSC Python\3064_Davis_labs\labs\lab_20_re_Syntax\address.data') as open_file:
            data = open_file.read()
    except IOError:
        print 'File could not be opened'
    # print raise_salary(data)
    # print names_numbers(data)
    # print california(data)
    # print month_of_birthday(data, 9)
    print '\n'.join(not_word(data))
    # print reverse_name(data)


if __name__ == '__main__':
    main()
