"""Money class that inherits from float
"""
import os
import sys

if __name__ == '__main__':
    sys.path.insert(0, '.')
else:
    sys.path.insert(0, os.path.join(os.path.split(__file__)[0], '..'))
from Lab8 import make_money


class Money(float):

    def __str__(self):
        return make_money(self)

    def __add__(self, other):
        return Money(float.__add__(self, other))

    def __sub__(self, other):
        return Money(float.__sub__(self, other))

    def __repr__(self):
        return self.__class__.__name__ + '(%f)' % self

    def __neg__(self):
        return Money(float.__neg__(self))

    def __mul__(self, other):
        return Money(float.__mul__(self, other))

    def __rmul__(self, other):
        return Money(float.__rmul__(self, other))

    def __div__(self, other):
        return Money(float.__div__(self, other))


def main():
    print Money(-123.21)
    print Money(40.50)
    print Money(-1001.011)
    print Money(123456789.999)
    print Money(.10)
    print Money(.01)
    print Money(.055)
    print 'repr:', eval(repr(Money(44.123))), ' == ', Money(44.123)
    print 'add:', Money(10) + Money(20), ' == ', Money(30)
    print 'repr:', eval(repr(Money(44.123))), ' == ', Money(44.123)
    print 'sub:', Money(44.333) - Money(55.444), ' == ', Money(-11.111)
    print 'neg:', -Money(10.00), ' == ', Money(-10.00)
    print 'mult:', 2 * Money(-11.11), ' == ', Money(-22.22), ' == ', Money(11.11) * -2
    print 'div:', (Money(44.44)) / 4, ' == ', Money(11.11)


if __name__ == '__main__':
    main()
