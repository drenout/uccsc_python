"""
Lab4 exercise3
Pass quiz based ob flashcard and you'll get a feedback
"""
import random


def praise():
    """Random praise phrases if flashcard is solved correctly"""

    phrases = ('Right On!', 'Good job', 'Excellent', 'Perfect!', 'Very good')
    # print phrases[random.randrange(5)]
    print random.choice(phrases)


def flashcard():
    """Function asks user to solve multiply task"""

    num1, num2 = random.randrange(13), random.randrange(13)
    while True:
        try:
            answer = int(raw_input('What is {0} times {1} '.format(num1, num2)))
            break
        except ValueError:
            print 'Please enter a number'
    if answer == num1 * num2:
        print 'Right!'
        praise()
        return 1
    else:
        print 'Almost, the right answer is {0}'.format(num1 * num2)
        return 0


def quiz(n):
    """Function give you several flashcards and return percent of correct answers"""

    score = 0
    for _ in range(n):
        score += flashcard()
    score = score * 100.0 / n
    print 'Score is {0}'.format(score)
    return score


def give_feedback(p):
    """Function provides feedback based on you quiz score"""

    if p == 100:
        print 'Perfect!'
    elif p in range(90, 100):
        print 'Excellent'
    elif p in range(80,90):
        print 'Very good'
    elif p in range(70,80):
        print 'Good enough'
    else:
        print 'You need more practice'


if __name__ == '__main__':
    percentage = quiz(10)
    give_feedback(percentage)
