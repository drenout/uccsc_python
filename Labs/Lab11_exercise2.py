"""
"""

import os
import sys
from apple.work_here import Lab11_exercise1 as totaler


def total_words_in_dir(root):
    num_of_files = total = 0
    for (cur_dir, sub_dir, files) in os.walk(root):
        for open_file in files:
            full_path = os.path.join(cur_dir, open_file)
            try:
                total += totaler.total_in_file(full_path)
                num_of_files += 1
            except IOError:
                print 'File couldn\'t be opened'
                exit()
    return num_of_files, total

def main():
    result = total_words_in_dir('..')
    print 'That\'s %d files, totaling to %.2f' % result


if __name__ == '__main__':
    main()

